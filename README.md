Task Manager
---
https://gitlab.com/AWTeterin/se08

Software
---
    -JRE
    -Java 1.8
    -Maven 4.0.0

Technology Stack
---
    -Maven
    -Git
    -Java SE

Developer
---
   Teterin Alexei    
   email: [teterin2012@rambler.ru](teterin2012@rambler.ru)

Commands for building the app
---
    mvn clean install
Commands for run the app
---
    java -jar target/taskmanager/bin/taskmanager.jar