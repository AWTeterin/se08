package ru.teterin.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;

public final class Constant {

    @NotNull
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static final String MD5 = "MD5";

    @NotNull
    public static final String START_MESSAGE = "*** WELCOME TO TASK MANAGER ***";

    @NotNull
    public static final String CORRECT_EXECUTION = "[OK]";

    @NotNull
    public static final String ENTER_PROJECT_ID = "ENTER PROJECT ID:";

    @NotNull
    public static final String ENTER_TASK_ID = "ENTER TASK ID:";

    @NotNull
    public static final String ENTER_NAME = "ENTER NAME:";

    @NotNull
    public static final String ENTER_DESCRIPTION = "ENTER DESCRIPTION:";

    @NotNull
    public static final String ENTER_START_DATE = "ENTER START DATE:";

    @NotNull
    public static final String ENTER_END_DATE = "ENTER END DATE:";

    @NotNull
    public static final String ENTER_LOGIN = "ENTER LOGIN:";

    @NotNull
    public static final String ENTER_NEW_LOGIN = "ENTER NEW LOGIN:";

    @NotNull
    public static final String ENTER_PASSWORD = "ENTER PASSWORD:";

    @NotNull
    public static final String ENTER_NEW_PASSWORD = "ENTER NEW PASSWORD:";

    @NotNull
    public static final String HELLO = "HELLO ";

    @NotNull
    public static final String BYE = "BYE BYE ";

    @NotNull
    public static final String USER = "USER: ";

    @NotNull
    public static final String ROLE = "ROLE: ";

    @NotNull
    public static final String PROJECT_CLEAR = "[PROJECTS CLEAR]";

    @NotNull
    public static final String PROJECT_CREATE = "[PROJECT CREATE]";

    @NotNull
    public static final String PROJECT_EDIT = "[PROJECT EDIT]";

    @NotNull
    public static final String PROJECT_LIST = "[PROJECT LIST]";

    @NotNull
    public static final String PROJECT_REMOVE = "[PROJECT REMOVE]";

    @NotNull
    public static final String PROJECT_TASKS = "[PROJECT TASK]";

    @NotNull
    public static final String ABOUT = "[ABOUT]";

    @NotNull
    public static final String TASK_CLEAR = "[TASK CLEAR]";

    @NotNull
    public static final String TASK_CREATE = "[TASK CREATE]";

    @NotNull
    public static final String TASK_EDIT = "[TASK EDIT]";

    @NotNull
    public static final String TASK_LINK = "[TASK_LINK]";

    @NotNull
    public static final String TASK_LIST = "[TASK LIST]";

    @NotNull
    public static final String TASK_REMOVE = "[TASK REMOVE]";

    @NotNull
    public static final String CHANGE_PASSWORD = "[CHANGE PASSWORD]";

    @NotNull
    public static final String USER_REGISTRATION = "[USER REGISTRATION]";

    @NotNull
    public static final String USER_EDIT = "[USER EDIT]";

    @NotNull
    public static final String USER_LOGIN = "[USER LOGIN]";

    @NotNull
    public static final String USER_LOGOUT = "[USER LOGOUT]";

    @NotNull
    public static final String USER_SHOW = "[USER SHOW]";

    @NotNull
    public static final String INVALID_COMMAND = "AN INVALID COMMAND WAS SUBMITTED IN THE COMMAND LIST!";

    @NotNull
    public static final String INCORRECT_COMMAND = "THE COMMAND IS NOT AVAILABLE!";

    @NotNull
    public static final String INCORRECT_PASSWORD = "INCORRECT PASSWORD! TRY A DIFFERENT PASSWORD!";

    @NotNull
    public static final String INCORRECT_ID = "INCORRECT FORMAT ID! TRY THIS FORMAT: 00000000-0000-0000-0000-000000000000";

    @NotNull
    public static final String INCORRECT_DATE = "INCORRECT FORMAT DATE! TRY THIS FORMAT: 01.01.2020";

    @NotNull
    public static final String USER_NOT_FOUND = "USER NOT FOUND! TRY A DIFFERENT LOGIN!";

    @NotNull
    public static final String OBJECT_EXIST = "OBJECT ALREADY EXIST! TRY CREATING A DIFFERENT OBJECT!";

    @NotNull
    public static final String OBJECT_NOT_FOUND = "OBJECT NOT FOUND!";

    @NotNull
    public static final String NO_COMMAND = "THE COMMAND CANNOT BE EMPTY!";

    @NotNull
    public static final String NO_PROJECT = "YOU CAN'T CREATE AN ISSUE WITHOUT A PROJECT! FIRST, CREATE A PROJECT!";

    @NotNull
    public static final String NO_ROLE = "THE ROLE CANNOT BE EMPTY!";

    @NotNull
    public static final String NO_PASSWORD = "THE PASSWORD CANNOT BE EMPTY!";

    @NotNull
    public static final String EMPTY_ID = "ID can't be empty!";

    @NotNull
    public static final String EMPTY_DATE = "Date can't be empty!";

    @NotNull
    public static final String EMPTY_OBJECT = "Object or its fields  cannot be empty!";

    @NotNull
    public static final String ERROR_OBJECT_ACCESS = "Object cannot be accessed!";

    @NotNull
    public static final String INCORRECT_COMMAND_INITIALIZATION = "INCORRECT COMMAND INITIALISATION! TERMINALSERVICE OR SERVICELOCATOR IS EMPTY!";

    @NotNull
    public static final String CREATED_BY = "Created-by";

    @NotNull
    public static final String BUILD_JDK = "Build-Jdk-Spec";

    @NotNull
    public static final String BUILD_NUMBER = "buildNumber";

    @NotNull
    public static final String DEVELOPER = "developer";

    public static final int WITHOUT_ERRORS = 0;

}
