package ru.teterin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(Constant.WITHOUT_ERRORS);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
