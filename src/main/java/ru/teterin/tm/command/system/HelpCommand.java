package ru.teterin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import java.util.Map;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        stateService = serviceLocator.getStateService();
        @NotNull final Map<String, AbstractCommand> commandMap = stateService.getCommands();
        for (@NotNull final AbstractCommand command : commandMap.values()) {
            @NotNull final String commandInfo = command.getName() + ": " + command.getDescription();
            terminalService.print(commandInfo);
        }
    }

    @Override
    public boolean secure() {
        return false;
    }

}
