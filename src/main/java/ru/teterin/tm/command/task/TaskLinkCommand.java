package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class TaskLinkCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-link";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Link an task to a project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_LINK);
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String projectId = terminalService.readString();
        terminalService.print(Constant.ENTER_TASK_ID);
        @Nullable final String taskId = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        taskService = serviceLocator.getTaskService();
        taskService.linkTask(userId, projectId, taskId);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
