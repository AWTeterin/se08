package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Task;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task for project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_CREATE);
        stateService = serviceLocator.getStateService();
        @Nullable String projectId = stateService.getProjectId();
        if (projectId == null) {
            terminalService.print(Constant.ENTER_PROJECT_ID);
            projectId = terminalService.readString();
        }
        if (projectId == null) {
            terminalService.print(Constant.NO_PROJECT);
            return;
        }
        @NotNull final Task task = terminalService.readTask();
        @Nullable final String userId = stateService.getUserId();
        task.setUserId(userId);
        task.setProjectId(projectId);
        taskService = serviceLocator.getTaskService();
        taskService.checkTask(userId, task);
        taskService.persist(userId, task);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
