package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Task;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Choose task by id and edit it.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_EDIT);
        terminalService.print(Constant.ENTER_TASK_ID);
        @Nullable final String id = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        taskService = serviceLocator.getTaskService();
        @NotNull final Task task = taskService.findOne(userId, id);
        terminalService.print(task);
        @NotNull final Task result = terminalService.readTask();
        result.setUserId(userId);
        result.setId(id);
        @Nullable final String projectId = task.getProjectId();
        result.setProjectId(projectId);
        taskService.checkTask(userId, result);
        taskService.merge(userId, result);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
