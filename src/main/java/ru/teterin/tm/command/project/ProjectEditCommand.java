package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;

public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Choose project by id and edit it.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_EDIT);
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String id = terminalService.readString();
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        projectService = serviceLocator.getProjectService();
        @NotNull final Project project = projectService.findOne(userId, id);
        terminalService.print(project);
        @NotNull final Project result = terminalService.readProject();
        result.setUserId(userId);
        result.setId(id);
        projectService.checkProject(userId, result);
        projectService.merge(userId, result);
        stateService.setProjectId(id);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
