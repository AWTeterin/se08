package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all project.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_CLEAR);
        projectService = serviceLocator.getProjectService();
        stateService = serviceLocator.getStateService();
        projectService.removeAll(stateService.getUserId());
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
