package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.enumerated.Role;

import java.util.Map;

public interface IStateService {

    @Nullable
    public String getLogin();

    public void setLogin(@Nullable final String login);

    @Nullable
    public Role getRole();

    public void setRole(@Nullable final Role role);

    @Nullable
    public String getUserId();

    public void setUserId(@Nullable final String userId);

    @Nullable
    public String getProjectId();

    public void setProjectId(@Nullable final String projectId);

    @NotNull
    public Map<String, AbstractCommand> getCommands();

    public void setCommands(@NotNull final Map<String, AbstractCommand> commands);

    @NotNull
    public AbstractCommand getCommand(@Nullable final String commandName);

    public void registryCommand(@NotNull final AbstractCommand command);

}
