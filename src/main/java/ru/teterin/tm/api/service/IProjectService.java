package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;

import java.util.Collection;

public interface IProjectService extends IService<Project> {

    @NotNull
    public Project checkProject(
        @Nullable final String userId,
        @NotNull final Project project
    );

    @NotNull
    public Collection<Task> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    );

}
