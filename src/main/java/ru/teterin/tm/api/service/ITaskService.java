package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Task;

public interface ITaskService extends IService<Task> {

    @NotNull
    public Task checkTask(
        @Nullable final String userId,
        @NotNull final Task task
    );

    @NotNull
    public Task linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    );

}
