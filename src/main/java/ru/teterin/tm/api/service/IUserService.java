package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.User;

public interface IUserService extends IService<User> {

    @NotNull
    public User create(@NotNull User user);

    @NotNull
    public User checkUser(
        @Nullable final String login,
        @Nullable final String password
    );

    public void changePassword(
        @Nullable final String login,
        @Nullable final String oldPassword,
        @Nullable final String newPassword
    );

    public void editUser(
        @Nullable final String oldLogin,
        @Nullable final String newLogin
    );

}
