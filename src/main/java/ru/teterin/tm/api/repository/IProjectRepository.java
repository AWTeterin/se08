package ru.teterin.tm.api.repository;

import ru.teterin.tm.entity.Project;

public interface IProjectRepository extends IRepository<Project> {

}
