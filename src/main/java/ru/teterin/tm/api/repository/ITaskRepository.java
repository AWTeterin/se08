package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository extends IRepository<Task> {

    public void removeAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    );

    @NotNull
    public Collection<Task> findAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    );

}
