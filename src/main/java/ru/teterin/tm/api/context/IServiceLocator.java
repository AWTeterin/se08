package ru.teterin.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.api.service.IUserService;

public interface IServiceLocator {

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public IStateService getStateService();

}
