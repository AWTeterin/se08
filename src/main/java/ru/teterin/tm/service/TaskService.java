package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.*;
import ru.teterin.tm.util.DateUuidParseUtil;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    public TaskService(
        @NotNull final ITaskRepository taskRepository,
        @NotNull final IProjectRepository projectRepository
    ) {
        super(taskRepository);
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public Task checkTask(
        @Nullable final String userId,
        @NotNull final Task task
    ) {
        @Nullable final String projectId = task.getProjectId();
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final String description = task.getDescription();
        if (description == null) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        task.setUserId(DateUuidParseUtil.isUuid(userId));
        task.setProjectId(DateUuidParseUtil.isUuid(projectId));
        return task;
    }

    @NotNull
    @Override
    public Task linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    ) {
        final boolean noProjectId = projectId == null || projectId.isEmpty();
        final boolean noTaskId = id == null || id.isEmpty();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noProjectId || noTaskId || noUserId) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final Project project = projectRepository.findOne(DateUuidParseUtil.isUuid(userId),
            DateUuidParseUtil.isUuid(projectId));
        @Nullable final Task task = findOne(userId, DateUuidParseUtil.isUuid(id));
        if (project == null) {
            throw new ObjectNotFoundException();
        }
        task.setProjectId(projectId);
        return task;
    }

}
