package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectService(
        @NotNull final IProjectRepository projectRepository,
        @NotNull final ITaskRepository taskRepository
    ) {
        super(projectRepository);
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Project remove(
        @Nullable final String userId,
        @Nullable final String id
    ) {
        @NotNull final Project project = super.remove(userId, id);
        if (userId != null && id != null) {
            taskRepository.removeAllByProjectId(userId, id);
        }
        return project;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        super.removeAll(userId);
        if (userId != null) {
            taskRepository.removeAll(userId);
        }
    }

    @NotNull
    @Override
    public Project checkProject(
        @Nullable final String userId,
        @NotNull final Project project
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final String description = project.getDescription();
        if (description == null) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        project.setUserId(DateUuidParseUtil.isUuid(userId));
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Collection<Task> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    ) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        final Project project = findOne(DateUuidParseUtil.isUuid(userId), DateUuidParseUtil.isUuid(id));
        if (project == null) {
            throw new ObjectNotFoundException();
        }
        return taskRepository.findAllByProjectId(userId, id);
    }

}
