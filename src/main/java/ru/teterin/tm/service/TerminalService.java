package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.service.ITerminalService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;
import java.util.Date;
import java.util.Scanner;

public final class TerminalService implements ITerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @Nullable
    @Override
    public String readString() {
        return scanner.nextLine();
    }

    @NotNull
    @Override
    public Project readProject() {
        @NotNull final Project project = new Project();
        System.out.println(Constant.ENTER_NAME);
        project.setName(scanner.nextLine());
        System.out.println(Constant.ENTER_DESCRIPTION);
        project.setDescription(scanner.nextLine());
        System.out.println(Constant.ENTER_START_DATE);
        @Nullable String start = scanner.nextLine();
        final boolean noStartDate = start == null || start.isEmpty();
        if (noStartDate) {
            throw new IllegalArgumentException(Constant.EMPTY_DATE);
        }
        @NotNull final Date startDate = DateUuidParseUtil.isDate(start);
        project.setDateStart(startDate);
        System.out.println(Constant.ENTER_END_DATE);
        @Nullable String end = scanner.nextLine();
        final boolean noEndDate = end == null || end.isEmpty();
        if (noEndDate) {
            throw new IllegalArgumentException(Constant.EMPTY_DATE);
        }
        @NotNull final Date endDate = DateUuidParseUtil.isDate(end);
        project.setDateEnd(endDate);
        return project;
    }

    @NotNull
    @Override
    public Task readTask() {
        @NotNull final Task task = new Task();
        System.out.println(Constant.ENTER_NAME);
        task.setName(scanner.nextLine());
        System.out.println(Constant.ENTER_DESCRIPTION);
        task.setDescription(scanner.nextLine());
        System.out.println(Constant.ENTER_START_DATE);
        @Nullable final String start = scanner.nextLine();
        final boolean noStartDate = start == null || start.isEmpty();
        if (noStartDate) {
            throw new IllegalArgumentException(Constant.EMPTY_DATE);
        }
        @NotNull final Date startDate = DateUuidParseUtil.isDate(start);
        task.setDateStart(startDate);
        System.out.println(Constant.ENTER_END_DATE);
        @Nullable final String end = scanner.nextLine();
        final boolean noEndDate = end == null || end.isEmpty();
        if (noEndDate) {
            throw new IllegalArgumentException(Constant.EMPTY_DATE);
        }
        @NotNull final Date endDate = DateUuidParseUtil.isDate(end);
        task.setDateEnd(endDate);
        return task;
    }

    @Override
    public void print(@Nullable Object message) {
        System.out.println(message);
    }

    @Override
    public void printCollection(@NotNull final Collection collection) {
        int count = 1;
        for (@NotNull final Object object : collection) {
            System.out.println(count + ". " + object);
            count++;
        }
    }

}
