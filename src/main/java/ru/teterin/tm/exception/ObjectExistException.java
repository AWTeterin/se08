package ru.teterin.tm.exception;

import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.constant.Constant;

public final class ObjectExistException extends RuntimeException {

    public ObjectExistException() {
        super(Constant.OBJECT_EXIST);
    }

    public ObjectExistException(@Nullable final String message) {
        super(message);
    }

}
