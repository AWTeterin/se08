package ru.teterin.tm;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.project.*;
import ru.teterin.tm.command.system.AboutCommand;
import ru.teterin.tm.command.system.ExitCommand;
import ru.teterin.tm.command.system.HelpCommand;
import ru.teterin.tm.command.task.*;
import ru.teterin.tm.command.user.*;
import ru.teterin.tm.context.Bootstrap;

public class Application {

    @NotNull
    private static final Class[] CLASSES = new Class[]{
        UserCreateCommand.class, UserEditCommand.class,
        UserChangePasswordCommand.class, UserLogInCommand.class,
        UserShowCommand.class, UserLogOutCommand.class,
        ProjectListCommand.class, ProjectTasksCommand.class,
        ProjectCreateCommand.class, ProjectEditCommand.class,
        ProjectRemoveCommand.class, ProjectClearCommand.class,
        TaskListCommand.class, TaskCreateCommand.class,
        TaskEditCommand.class, TaskLinkCommand.class,
        TaskRemoveCommand.class, TaskClearCommand.class,
        HelpCommand.class, AboutCommand.class,
        ExitCommand.class
    };

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }

}
