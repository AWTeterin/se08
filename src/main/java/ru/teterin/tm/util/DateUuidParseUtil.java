package ru.teterin.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.constant.Constant;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

public final class DateUuidParseUtil {

    @NotNull
    public static String isUuid(@NotNull String id) {
        try {
            @NotNull final UUID uuid = UUID.fromString(id);
            id = uuid.toString();
            return id;
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(Constant.INCORRECT_ID);
        }
    }

    @NotNull
    public static Date isDate(@NotNull String stringDate) {
        try {
            @NotNull final Date date = Constant.DATE_FORMAT.parse(stringDate);
            return date;
        } catch (ParseException e) {
            throw new IllegalArgumentException(Constant.INCORRECT_DATE);
        }
    }

}
