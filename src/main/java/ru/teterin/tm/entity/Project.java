package ru.teterin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractEntity {

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateEnd;

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", dateStart=" + dateStart +
            ", dateEnd=" + dateEnd +
            '}';
    }

}
