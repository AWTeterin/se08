package ru.teterin.tm.repository;

import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.entity.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

}
